<?php
session_start();
$email = "";
$password = "";
$salt = 'XyZzy12*_';
$stored_hash = '1a52e17fa899cf40fb04cfc42e6352f1';
$md5 = '';
$mensaje = array();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    if (isset($_POST['email'])) {
        $email = htmlspecialchars(trim($_POST['email']));
    }

    if (isset($_POST['password'])) {
        $password = htmlspecialchars(trim($_POST['password']));
    }

    if (empty($email)) {
        $mensaje['email'] = 'El campo Email está vacio.';
    } 
    
    if (empty($password)) {
        $mensaje['password'] = 'El campo Password está vacio';
    }


    if (!empty($password) && !empty($email)) {
        $md5 = hash('md5', $salt . $password);
        if ($md5 != $stored_hash && !filter_var($email, FILTER_VALIDATE_EMAIL)) {

            $_SESSION['errorEmail'] = "Email must have an at-sign (@)";
            $_SESSION['errorPass'] = 'Password invalid';
            error_log("Login fail " . $_POST['email'] . " $md5");
            header("Location: login.php");
            return;

        } else if ($md5 != $stored_hash) {

            $_SESSION['errorPass'] = 'Password invalid';
            error_log("Login fail " . $_POST['email'] . " $md5");
            header("Location: login.php");
            return;
        } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {

            $_SESSION['errorEmail'] = "Email must have an at-sign (@)";
            error_log("Login fail " . $_POST['email'] . " $md5");
            header("Location: login.php");
            return;
        } else {
            $_SESSION['name'] = $email;
            error_log("Login success " . $_POST['email']);
            $_SESSION['log-in'] = "Log in";
            header("Location: autos.php");
            return;
        }
    }
}

function mostrarErrorer($men, $nom)
{
    if (!empty($men)) {
        return !empty($men[$nom]) ? $men[$nom] : null;
    }
}

require "views/login.view.php";
