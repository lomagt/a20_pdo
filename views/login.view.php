<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Login</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="bootstrap-5.1.3-dist/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="d-flex justify-content-center mb-3">
            <div class="login text-center">
                <h1 class="text-white">Log in</h1>
            </div>
        </div>
        <div class="contentForm d-flex justify-content-center   ">
            <form action="" method="post">
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label d-flex align-items-start">Email address</label>
                    <input type="text" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div>
                    <p class="text-danger">
                        <?php 
                        if(isset($_SESSION['errorEmail'])){
                            echo htmlentities($_SESSION['errorEmail']);
                            unset($_SESSION['errorEmail']);
                        }

                        echo mostrarErrorer($mensaje,'email')
                        ?>
                    </p>
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label d-flex align-items-start">Password</label>
                    <input type="password" class="form-control" name="password" id="exampleInputPassword1">
                </div>
                <div>
                    <p class="text-danger">
                        <?php
                            echo mostrarErrorer($mensaje,'password'); 
                            
                            if(isset($_SESSION['errorPass'])){
                                echo htmlentities($_SESSION['errorPass']);
                                unset($_SESSION['errorPass']);
                            }
                            
                        ?>
                    </p>
                </div>
                <div>
                    <button type="submit" class="btn btn-primary d-flex align-items-start">Submit</button>
                </div>    
            </form>
        </div>            
    </div>
    <script src="bootstrap-5.1.3-dist/js/bootstrap.min.js"></script>
</body>
</html>