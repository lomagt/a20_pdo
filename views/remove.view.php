<!-- Head -->
<?php require "partials/inicio-doc.part.php"; ?>
<!-- End Head -->
<div class="d-flex justify-content-center mt-5">
    <h1 class="text-capitalize text-decoration-underline text-info">Eliminar registro</h1>
</div>
<div class="d-flex justify-content-center mt-5">
    <h1 class="text-capitalize text-danger">Eliminar <?=$marca?> del año <?=$year?> con <?=$mileage?>kms</h1>
</div>
<form class="d-flex justify-content-center mt-5" action="" method="post">
    <button class="btn btn-outline-danger mx-3 p-3" type="submit" name="remove">Eliminar</button>
    <button class="btn btn-outline-primary p-3" type="submit" name="cancel">Cancelar</button>
</form>

<!-- Fin HTML y Scripts -->
<?php require "partials/fin-doc.part.php"; ?>
<!-- Fin Document -->