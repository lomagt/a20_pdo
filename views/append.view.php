<!-- Head -->
<?php require "partials/inicio-doc.part.php"; ?>
<!-- End Head -->

<div class="d-flex justify-content-center mt-5">
    <h1 class="text-capitalize text-decoration-underline text-info">Añadir nuevo registro</h1>
</div>

<!-- Comienzo del formulario de inserciones -->
<div class="container-fluid inputs">
    <form class="mt-3" action="" method="post">
      <div class="form-floating mb-3">
        <input type="text" class="form-control" id="floatingInput" placeholder="Marca" name="marca">
        <label for="floatingInput">Make</label>
      </div>
      <div class="form-floating mb-3">
        <input type="text" class="form-control" id="floatingYear" placeholder="year" name="year">
        <label for="floatingPassword">Year</label>
      </div>
      <div class="form-floating mb-3">
        <input type="text" class="form-control" id="floatingMileage" placeholder="Mileage" name="mileage">
        <label for="floatingPassword">Mileage</label>
      </div>
        <div class="row gx-2">
            <div class="col-1">
                <button class="btn btn-primary h-100" type="submit" name="append">Add Register</button>
            </div>
            <div class="col-1">
                <button class="btn btn-danger h-100" type="submit" name="cancel">Cancelar</button>
            </div>
        </div>
    </form>
</div>
    <!-- Final del formulario de inserciones -->

     <!-- Comienzo del Div que contiene los Mensajes -->
     <div class="mensajesAutos mt-5 mb-4">
      <?php

if (isset($_SESSION['success2'])) {
    echo '<p class="text-danger text-center">' . htmlentities($_SESSION['success2']) . '<p>';
    unset($_SESSION['success2']);
}

?>
    </div>
   <!-- Cierre del Div que contiene los Mensajes -->

<!-- Fin HTML y Scripts -->
<?php require "partials/fin-doc.part.php"; ?>
<!-- Fin Document -->