<!-- Head -->
<?php require "partials/inicio-doc.part.php"; ?>
<!-- End Head -->
<div class="d-flex justify-content-center mt-5">
    <h1 class="text-capitalize text-decoration-underline text-info">Editar registro</h1>
</div>
<form class="d-flex justify-content-center mt-5" action="" method="post">

    <div class="mx-2">
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="floatingMake" name="marca" value="<?php echo $temObj->getMake();?>">
            <label for="floatingMake">Make</label>
        </div>
        <div class="mb-2 mt-2">
            <p class="text-danger"></p>
        </div>
    </div>

    <div class="mx-2">
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="floatingYear" name="year" value="<?php echo $temObj->getYear();?>">
            <label for="floatingYear">Year</label>
        </div>
        <div class="mb-2 mt-2">
            <p class="text-danger"></p>
        </div>
    </div>

    <div class="mx-2">
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="floatingMileage" name="mileage" value="<?php echo $temObj->getMileage();?>">
            <label for="floatingMileage">Mileage</label>
        </div>
        <div class="mb-2 mt-2">
            <p class="text-danger"></p>
        </div>
    </div>
    
    <button class="btn btn-outline-danger mx-3 p-3" type="submit" name="update">Actualizar</button>
    <button class="btn btn-outline-primary p-3" type="submit" name="cancel">Cancelar</button>
</form>
<div class="d-flex justify-content-center mt-5">
    <?php
        if (isset($_SESSION['errorNum'])) {
            echo '<p class="text-danger">' . htmlentities($_SESSION['errorNum']) . '<p>';
        }
        
        if (isset($_SESSION['errorEmpty2'])) {
            echo '<p class="text-success">' . htmlentities($_SESSION['errorEmpty2']) . '<p>';
        }

        if (isset($_SESSION['borrarSession'])) {
            
            unset($_SESSION['errorNum']);
            unset($_SESSION['errorEmpty2']);
        }
         
    ?>
</div>
<!-- Fin HTML y Scripts -->
<?php require "partials/fin-doc.part.php"; ?>
<!-- Fin Document -->