<!-- Head -->
<?php require "partials/inicio-doc.part.php";?>
<!-- End Head -->
  <div class="container">
    <header class="p-3 mt-3 text-white ">
      <h1>Bienvenido a la página de administración</h1>
    </header>
    <nav class="navbar navbar-expand-lg navbar-light bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand text-white" href="autos.php">Administración</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse align-items-center" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link active text-white" aria-current="page" href="#">Home</a>
            </li>
          </ul>
          <form class="row gx-2 d-flex" method="post">
            <div class="col">
      <?php
if (isset($_SESSION['name'])) {
    echo '<button class="btn btn-success h-100">' . htmlentities($_SESSION['name']) . '</button>';}
?>
            </div>
            <div class="col">
                <button class="btn btn-outline-primary h-100" type="submit" name="add">Add Register</button>
            </div>
            <div class="col">
                <button class="btn btn-outline-info h-100" type="submit" name="close">Log out</button>
            </div>
          </form>
        </div>
      </div>
    </nav>

    <!-- Comienzo del Div que contiene la tabla -->
    <div class="container-fluid boxTable d-flex justify-content-center mt-5">
      <div class="cubierta">
        <table id="plantilla" class="table table-success table-hover">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Make</th>
              <th scope="col">Year</th>
              <th scope="col">Mileage</th>
              <th scope="col">Delete</th>

            </tr>
          </thead>
          <tbody>
          <?php
$cont = 1;
foreach ($query as $row) {

    ?>

      <tr>
        <th scope="row"><?=$cont?></th>
        <td><?=$row->getMake()?></td>
        <td><?=$row->getYear()?></td>
        <td><?=$row->getMileage()?></td>
        <td>
          <div class="d-flex justify-content-start">
            <form action="remove.php" method='get' class="mx-2">
              <button class="btn btn-outline-danger" type='submit' name="delete" value="<?=$row->auto_id?>">Delete</button>
            </form>
            <form action="edit.php" method='get'>
              <button class="btn btn-outline-success" type='submit' name="update" value="<?=$row->auto_id?>">Update</button>
            </form>
          </div>
        </td>
      </tr>

    <?php
$cont++;
}
?>
          </tbody>
        </table>
      </div>
    </div>
    <!-- Cierre del Div que contiene la tabla -->

    <!-- Comienzo del Div que contiene los Mensajes -->
    <div class="mensajesAutos mt-5 mb-4">
      <?php
if (isset($_SESSION['vacio'])) {
    echo '<p class="text-primary text-center">' . htmlentities($_SESSION['vacio']) . '<p>';
    unset($_SESSION['vacio']);
}

if (isset($_SESSION['success'])) {
    echo '<p class="text-success text-center">' . htmlentities($_SESSION['success']) . '<p>';
    unset($_SESSION['success']);
}

if (isset($_SESSION['success_del'])) {
    echo '<p class="text-success text-center">' . htmlentities($_SESSION['success_del']) . '<p>';
    unset($_SESSION['success_del']);
}

if (isset($_SESSION['update-success'])) {
    echo '<p class="text-success text-center">' . htmlentities($_SESSION['update-success']) . '<p>';
    unset($_SESSION['update-success']);
}

?>
    </div>
   <!-- Cierre del Div que contiene los Mensajes -->

  </div>
<!-- Fin HTML y Scripts -->
<?php require "partials/fin-doc.part.php";?>
<!-- Fin Document -->
