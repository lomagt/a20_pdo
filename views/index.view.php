<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PDO: Conexión, inserción y borrado</title>
    <link rel="stylesheet" href="bootstrap-5.1.3-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div class="container">
        <h1 class="text-white headH1 p-3 mb-3">Bienvenido a tu página de administración</h1>
        <a href="login.php">Please Log in</a>
    </div>

    <script src="bootstrap-5.1.3-dist/js/bootstrap.min.js"></script>
</body>
</html>