<?php

class Auto
{

    private $db;

    private $make;

    private $year;

    private $mileage;

    public function __construct($make = '', $year = 0, $mileage = 0)
    {
        $this->$make = $make;
        $this->year = $year;
        $this->mileage = $mileage;
    }

    public function makeConnection()
    {

        $this->db = new Database();

    }

    public function getAutos()
    {

        $this->db->query("SELECT auto_id, make, year, mileage FROM autos");

        $results = $this->db->resultSet('Auto');

        return $results;
    }

    public function addAuto($auto)
    {

        $this->db->query('insert into autos (make,year,mileage) Values (:mk,:yr,:mi)');

        $this->db->bind(':mk', $auto->getMake());

        $this->db->bind(':yr', $auto->getYear());

        $this->db->bind(':mi', $auto->getMileage());

        $this->db->execute();

    }

    public function delAuto($id)
    {
        $this->db->query('DELETE FROM autos WHERE auto_id = :id');
        
        $this->db->bind(':id', $id);

        $this->db->execute();
    }

    public function showSingle($id){

        $this->db->query("SELECT auto_id, make, year, mileage FROM autos Where auto_id = :id");

        $this->db->bind(':id', $id);

        return $this->db->single('Auto');
    }

    public function updateAuto($auto){

        $this->db->query('UPDATE autos SET make = :mk, year = :y, mileage = :mlg WHERE auto_id = :id');

        $this->db->bind(':mk', $auto->getMake());
        $this->db->bind(':y', $auto->getYear());
        $this->db->bind(':mlg', $auto->getMileage());
        $this->db->bind(':id', $auto->auto_id);

        $this->db->execute();
    }

    public function rowCount(){
        $this->db->query("SELECT * FROM autos");

        $count = $this->db->resultSet('Auto');

        return count($count);
    }

    /**
     * Get the value of make
     */
    public function getMake()
    {
        return $this->make;
    }

    /**
     * Set the value of make
     *
     * @return  self
     */
    public function setMake($make)
    {
        $this->make = $make;

        return $this;
    }

    /**
     * Get the value of year
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set the value of year
     *
     * @return  self
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get the value of mileage
     */
    public function getMileage()
    {
        return $this->mileage;
    }

    /**
     * Set the value of mileage
     *
     * @return  self
     */
    public function setMileage($mileage)
    {
        $this->mileage = $mileage;

        return $this;
    }
}
