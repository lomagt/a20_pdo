<?php
session_start();

require "config/config.php";
require "lib/Database.php";
require "models/Auto.php";


$errorMessage = [];
$modelo = new Auto();
$modelo->makeConnection();

$query = $modelo->getAutos();

if (empty($query)) {
    $_SESSION['vacio'] = 'No se encontraron filas';
}

if (!isset($_SESSION['name'])) {
    die('Not logged in');
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    
    if (isset($_POST['close'])) {

        header("Location: logout.php") and die();
    }

    if (isset($_POST["add"])) {

        header("Location: append.php") and die();        
    }

}

require "views/autos.view.php";
