<?php

session_start();

require "config/config.php";
require "lib/Database.php";
require "models/Auto.php";

$marca = '';
$year = '';
$mileage = '';

$model = new Auto();
$model->makeConnection();

if (!isset($_SESSION['name'])) {
    die('ACCESS DENIED');
}

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (isset($_GET['delete'])) {
        $autoDel = $model->getAutos();
        foreach ($autoDel as $obj) {
            if ($_GET['delete'] == $obj->auto_id) {
                $marca = $obj->getMake();
                $year = $obj->getYear();
                $mileage = $obj->getMileage();
            }
        }
        $_SESSION['del_id'] = $_GET['delete'];
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    if (isset($_POST['remove'])) {

        $model->delAuto($_SESSION['del_id']);
        $_SESSION['success_del'] = 'Registro borrado';
        unset($_SESSION['del_id']);
        header('Location: autos.php');
        return;
    }

    if (isset($_POST['cancel'])) {

        unset($_SESSION['del_id']);
        header('Location: autos.php');
        return;
    }

}

require "views/remove.view.php";
