<?php
session_start();

require "config/config.php";
require "lib/Database.php";
require "models/Auto.php";

$marca = '';
$year = '';
$mileage = '';
$errorMessage = [];
$modelo = new Auto();
$modelo->makeConnection();

if (!isset($_SESSION['name'])) {
    die('ACCESS DENIED');
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    if(isset($_POST['cancel'])){
        
        header("Location: autos.php");
        return;
    }

    if (isset($_POST['marca'])) {
        $marca = htmlspecialchars(trim($_POST['marca']));
    }

    if (isset($_POST['year'])) {
        $year = htmlspecialchars(trim($_POST['year']));
    }

    if (isset($_POST['mileage'])) {
        $mileage = htmlspecialchars(trim($_POST['mileage']));
    }

    if (empty($marca) || is_numeric($marca)) {
        
        $_SESSION['success2'] = "Error insert";
        header("Location: append.php");
        return;
    }

    if ( empty($year) || !is_numeric($year)) {
        
        $_SESSION['success2'] = "Error insert";
        header("Location: append.php");
        return;
    }

    if (empty($mileage) || !is_numeric($mileage)) {
        
        $_SESSION['success2'] = "Error insert";
        header("Location: append.php");
        return;
    }

    if (!empty($marca) && is_numeric($year) && is_numeric($mileage)) {
        $modelo->setMake($marca);
        $modelo->setYear($year);
        $modelo->setMileage($mileage);

        $modelo->addAuto($modelo);
        $_SESSION['success'] = "Record inserted";
        header("Location: autos.php");
        return;

    }

}


require "views/append.view.php";