<?php

session_start();

require "config/config.php";
require "lib/Database.php";
require "models/Auto.php";

$marca = '';
$year = '';
$mileage = '';
$errorMessage = [];
$model = new Auto();
$model->makeConnection();
$_SESSION['borrarSession'] = 'Borrame';

if (!isset($_SESSION['name'])) {
    die('ACCESS DENIED');
}
if (isset($_GET['update'])) {
    $temObj = $model->showSingle($_GET['update']);
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    if (isset($_POST['marca'])) {
        $marca = htmlspecialchars(trim($_POST['marca']));
    }

    if (isset($_POST['year'])) {
        $year = htmlspecialchars(trim($_POST['year']));
    }

    if (isset($_POST['mileage'])) {
        $mileage = htmlspecialchars(trim($_POST['mileage']));
    }

    if (isset($_POST['cancel'])) {

        header('location: autos.php') and die();
    }

    if (isset($_POST['update'])) {

        if (empty($marca) && empty($year) && empty($mileage)) {

            unset($_SESSION['errorNum']);
            unset($_SESSION['borrarSession']);
            $_SESSION['errorEmpty2'] = 'Sin datos insertados para actualizar';
            header('location: edit.php?update=' . urlencode($_GET['update'])) and die();

        } else if (is_numeric($marca) && !empty($marca) || !is_numeric($year) && !empty($year) || !is_numeric($mileage) && !empty($mileage)) {

            unset($_SESSION['errorEmpty2']);
            unset($_SESSION['borrarSession']);
            $_SESSION['errorNum'] = 'Error en los datos insertados';
            header('location: edit.php?update=' . urlencode($_GET['update'])) and die();

        } else if (!empty($marca) && !empty($year) && !empty($mileage)) {

            $temObj->setMake($marca);
            $temObj->setYear($year);
            $temObj->setMileage($mileage);
            $model->updateAuto($temObj);
            unset($_SESSION['borrarSession']);
            $_SESSION['update-success'] = 'Resgistro modificado';
            header('location: autos.php') and die();
        }

    }

}

require "views/edit.view.php";


